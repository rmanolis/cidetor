import requests
import zipfile
import os
import string
from .models import Word
from mongoengine import DoesNotExist
import xml.etree.ElementTree as ET
from cidetor.settings import BASE_DIR

LOCAL_FILENAME = BASE_DIR + '/new-entries.zip'
LOCAL_FOLDER = BASE_DIR + "/new-entries"


def download_gcide():
    if not os.path.isfile(LOCAL_FILENAME):
        r = requests.get(
            'http://rali.iro.umontreal.ca/GCIDE/new-entries.zip', stream=True)
        with open(LOCAL_FILENAME, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    # f.flush() commented by recommendation from J.F.Sebastian
        return LOCAL_FILENAME


def unzip_gcide():
    if not os.path.isdir(LOCAL_FOLDER):
        zip_ref = zipfile.ZipFile(LOCAL_FILENAME, 'r')
        zip_ref.extractall('.')
        zip_ref.close()


def upload_xml_to_db():
    # Download the gcide xml files before starting
    download_gcide()
    unzip_gcide()
    for c in string.ascii_lowercase:
        tree = ET.parse(LOCAL_FOLDER + '/gcide_' + c + '-entries.xml')
        root = tree.getroot()
        for entry in root:
            key = entry.get('key')
            try:
                Word.objects(key=key).get()
            except DoesNotExist:
                word = Word(key=key, letter=c, xml=ET.tostring(entry).decode(encoding='UTF-8'))
                word.save()
