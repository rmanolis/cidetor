from mongoengine import Document, StringField
import xml.etree.ElementTree as ET

class Word(Document):
    key = StringField(max_length=255)
    letter = StringField(max_length=2)
    xml = StringField()

    def serialize_xml(self):
        serial = {}
        entry = ET.fromstring(self.xml)
        serial['key'] = entry.get('key')
        definition = entry.find('def')
        if definition is not None:
            serial['definition'] = ET.tostring(definition).decode(encoding='UTF-8')
        
        return serial
