from django.conf.urls import url
from . import views

app_name = 'words'

urlpatterns = [
    url(r'^words/load_words$', views.LoadWordsFromCideView.as_view(), name='load_words'),
    url(r'^words/search$', views.SearchView.as_view(), name='search'),

]