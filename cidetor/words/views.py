from django.shortcuts import render
from django.views import View
from words import xml_to_db
from .models import Word


class LoadWordsFromCideView(View):

    def get(self, request):
        return render(request, 'load_words.html') 
    
    def post(self, request):
        xml_to_db.upload_xml_to_db()
        return render(request, 'load_words.html',
                              {'message': "Words have been loaded"})


class SearchView(View):
    def get(self, request):
        return render(request, 'search.html') 
    
    def post(self, request):
        search = request.POST['search']
        try:
            words = Word.objects(key__istartswith=search)
            serials = []
            for word in words:
                serials.append(word.serialize_xml())

            return render(request, 'search.html',
                              {'words': serials,
                              'search':search})
        except Exception as e:
            return render(request, {'error_message':e} )