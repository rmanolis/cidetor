import xml.etree.ElementTree as ET
from scrapy.selector import Selector

tree = ET.parse('new-entries/gcide_a-entries.xml')
root = tree.getroot()


for entry in root:
    hw_xml = entry.find('hw')
    name = entry.get('key')
    try:
        Word.get(Word.name == name)
    except:
        word = Word.create(name=name)
        if hw_xml is not None:
            word.hw = hw_xml.text

        definition = entry.find('def')
        if definition is not None:
            word.definition = definition.text

        word.save()
    else:
        print(name, "is already saved")
'''

#words = root.findall(".//entry[@key='Azure']")
# for word in words:
#    print(str(ET.tostring(word).decode(encoding='UTF-8')))

sls = Selector(text=ET.tostring(root)
               .decode(encoding='UTF-8')) \
               .xpath("//entry[starts-with(@key,'Az')]") \
               .extract()

for sl in sls:
    print(sl)
'''