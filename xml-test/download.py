import requests
import zipfile
def download_gcide():
    local_filename = 'new-entries.zip'
    r = requests.get('http://rali.iro.umontreal.ca/GCIDE/new-entries.zip', stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024): 
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                #f.flush() commented by recommendation from J.F.Sebastian
    return local_filename


download_gcide()
zip_ref = zipfile.ZipFile('new-entries.zip','r')
zip_ref.extractall('.')
zip_ref.close()